package fr.esgi.monopoly;

import fr.esgi.monopoly.player.Player;
import fr.esgi.monopoly.property.Property;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Monopoly {
    private final List<Property> properties = new ArrayList<>();

    public List<Property> properties() {
        return properties;
    }

    public void addProperty(Property property) {
        properties.add(property);
    }

    public Optional<Property> find(String propertyName) {
        return properties().stream().filter(p -> p.name().equals(propertyName)).findFirst();
    }

    public void buy(final Player currentPlayer, final Property landingProperty) {
        // Nous n'allons pas développer la feature pour gérer le cas ou le joueur n'a pas assez d'argent
        currentPlayer.setBalance(currentPlayer.balance() - landingProperty.price());
        currentPlayer.receiveTitleDeed(landingProperty);
    }
}
