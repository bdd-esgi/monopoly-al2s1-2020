package fr.esgi.monopoly.property;

import java.util.Objects;

public class Property {
    private final String name;
    private int price;

    public Property(final String name, final int price) {
        this.name = name;
        this.price = price;
    }

    public String name() {
        return name;
    }

    public int price() {
        return price;
    }

    public void setPrice(final int propertyPrice) {
        this.price = propertyPrice;
    }

    @Override
    public String toString() {
        return name + " " + price + " €";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Property)) return false;
        final Property property = (Property) o;
        return Objects.equals(name, property.name) && Objects.equals(price, property.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price);
    }
}
