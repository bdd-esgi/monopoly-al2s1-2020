package fr.esgi.monopoly.player;

import fr.esgi.monopoly.property.Property;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private int balance = 0;
    private List<Property> ownedProperties = new ArrayList<>();

    public int balance() {
        return balance;
    }

    public void setBalance(final int balance) {
        this.balance = balance;
    }

    public void receiveTitleDeed(final Property property) {
        ownedProperties.add(property);
    }

    public List<Property> ownedProperties() {
        return new ArrayList<>(ownedProperties);
    }
}
