package fr.esgi.feature;

import fr.esgi.monopoly.Monopoly;
import fr.esgi.monopoly.player.Player;
import fr.esgi.monopoly.property.Property;

public class World {
    private final Monopoly monopoly;
    private Property landingProperty;
    private Player currentPlayer;

    public World(final Monopoly monopoly) {
        this.monopoly = monopoly;
    }

    public Monopoly monopoly() {
        return monopoly;
    }

    public Property landingProperty() {
        return landingProperty;
    }

    public void setLandingProperty(final Property landingProperty) {
        this.landingProperty = landingProperty;
    }

    public Player currentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(final Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
}
