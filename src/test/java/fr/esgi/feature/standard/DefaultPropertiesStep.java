package fr.esgi.feature.standard;

import fr.esgi.feature.World;
import fr.esgi.monopoly.property.Property;
import io.cucumber.java.Before;
import io.cucumber.java.DataTableType;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.Assertions;

import java.util.List;
import java.util.Map;

public class DefaultPropertiesStep {

    private final World world;
    private Scenario scenario;

    // Ici Pico container va injecter un singleton de World
    public DefaultPropertiesStep(final World world) {
        this.world = world;
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @Given("default properties are used")
    public void defaultPropertiesAreUsed() {
        createDefaultProperties();
    }

    @Before("@default_properties")
    public void createDefaultProperties() {
        world.monopoly().addProperty(new Property("west", 200));
        world.monopoly().addProperty(new Property("east", 50));
    }

    @Then("the available properties should be at least:$")
    public void theAvailablePropertiesShouldBeAtLeast(List<Property> properties) {
        Assertions.assertThat(world.monopoly().properties()).containsAll(properties);
    }

    @DataTableType
    public Property property(Map<String, String> entry) {
        return new Property(
                entry.get("name"),
                Integer.parseInt(entry.get("price")));
    }
}
