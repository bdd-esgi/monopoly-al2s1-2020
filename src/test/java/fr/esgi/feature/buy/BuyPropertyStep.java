package fr.esgi.feature.buy;

import fr.esgi.feature.World;
import fr.esgi.monopoly.property.Property;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

public class BuyPropertyStep {

    private final World world;

    // Ici Pico container va injecter un singleton de World
    public BuyPropertyStep(final World world) {
        this.world = world;
    }

    @And("I land on unowned {string} property that cost {int}")
    public void iLandOnUnownedPropertyThatCost(String propertyName, int propertyPrice) {
        final Property property = world.monopoly().find(propertyName).orElseThrow();
        property.setPrice(propertyPrice);
        world.setLandingProperty(property);
    }

    @When("I buy the property")
    public void iBuyTheProperty() {
        world.monopoly().buy(world.currentPlayer(), world.landingProperty());
    }

    @Then("I should receive the {string} title deed")
    public void iShouldReceiveTheTitleDeed(String name) {
        Assertions.assertThat(world.currentPlayer().ownedProperties().stream().map(Property::name)).contains(name);
    }
}
