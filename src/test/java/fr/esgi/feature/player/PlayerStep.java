package fr.esgi.feature.player;

import fr.esgi.feature.World;
import fr.esgi.monopoly.player.Player;
import io.cucumber.java.en.And;
import org.assertj.core.api.Assertions;

public class PlayerStep {

    private final World world;

    // Ici Pico container va injecter un singleton de World
    public PlayerStep(final World world) {
        this.world = world;
    }

    @And("my funds are {int}")
    public void myFundsAre(int balance) {
        final Player player = new Player();
        player.setBalance(balance);
        world.setCurrentPlayer(player);
    }

    @And("my funds should be {int}")
    public void myFundsShouldBe(int balance) {
        Assertions.assertThat(world.currentPlayer().balance()).isEqualTo(balance);
    }
}
