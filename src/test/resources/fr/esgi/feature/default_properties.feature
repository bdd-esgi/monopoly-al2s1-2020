Feature: default properties definition

  Scenario: Default properties
    Given default properties are used
    Then the available properties should be at least:
      | name | price |
      | west | 200   |
      | east | 50    |
