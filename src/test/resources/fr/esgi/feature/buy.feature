@default_properties
Feature: Buy properties with default properties

  Scenario: Buying property
    Given I land on unowned "west" property that cost 180
    And my funds are 2000
    When I buy the property
    Then I should receive the "west" title deed
    And my funds should be 1820

  Scenario Outline:
    Given I land on unowned <name> property that cost <price>
    And my funds are <funds>
    When I buy the property
    Then I should receive the <name> title deed
    And my funds should be <remainedFunds>

    Examples:
      | name   | price | funds | remainedFunds |
      | "west" | 180   | 2000  | 1820          |
      | "east" | 100   | 2000  | 1900          |



